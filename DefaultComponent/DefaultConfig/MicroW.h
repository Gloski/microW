/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MicroW
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\MicroW.h
*********************************************************************/

#ifndef MicroW_H
#define MicroW_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
class IEngine;

//## auto_generated
class Magnetron;

//## auto_generated
class Rotor;

//## classInstance itsSterownik
class Sterownik;

//## auto_generated
class Wyswietlacz;

//#[ ignore
#define evStart_MicroW_id 18601

#define evStop_MicroW_id 18602

#define evDoorOpened_MicroW_id 18603

#define evDoorClosed_MicroW_id 18604

#define evDone_MicroW_id 18605

#define evDateChanged_MicroW_id 18606

#define changeHour_MicroW_id 18607

#define evChangeHour_MicroW_id 18608

#define evTimeTick_MicroW_id 18609

#define evChangedDispMode_MicroW_id 18610
//#]

//## package MicroW


//## type PowerMode
enum PowerMode {
    FREEZ = 0,
    SLOW = 1,
    MID = 2,
    FAST = 3,
    GRILL = 4,
    TOTAL_N = 5 // count all
};

//## attribute TIME_SCALE
static int TIME_SCALE(1);

//## classInstance itsSterownik
extern Sterownik itsSterownik;

//## auto_generated
void MicroW_initRelations();

//## auto_generated
bool MicroW_startBehavior();

//## type DisplayMode
enum DisplayMode {
    OFF = 0,
    STARTING = 1,
    RUNNING = 2,
    STOPPED = 3,
    FINISHED = 4
};

//#[ ignore
class MicroW_OMInitializer {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    MicroW_OMInitializer();
    
    //## auto_generated
    ~MicroW_OMInitializer();
};
//#]

//## event evStart()
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStart();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evDoorOpened()
class evDoorOpened : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDoorOpened;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDoorOpened();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDoorOpened : virtual public AOMEvent {
    DECLARE_META_EVENT(evDoorOpened)
};
//#]
#endif // _OMINSTRUMENT

//## event evDoorClosed()
class evDoorClosed : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDoorClosed;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDoorClosed();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDoorClosed : virtual public AOMEvent {
    DECLARE_META_EVENT(evDoorClosed)
};
//#]
#endif // _OMINSTRUMENT

//## event evDone()
class evDone : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDone;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDone();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDone : virtual public AOMEvent {
    DECLARE_META_EVENT(evDone)
};
//#]
#endif // _OMINSTRUMENT

//## event evDateChanged()
class evDateChanged : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDateChanged;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDateChanged();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDateChanged : virtual public AOMEvent {
    DECLARE_META_EVENT(evDateChanged)
};
//#]
#endif // _OMINSTRUMENT

//## event changeHour()
class changeHour : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedchangeHour;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    changeHour();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedchangeHour : virtual public AOMEvent {
    DECLARE_META_EVENT(changeHour)
};
//#]
#endif // _OMINSTRUMENT

//## event evChangeHour()
class evChangeHour : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevChangeHour;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evChangeHour();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevChangeHour : virtual public AOMEvent {
    DECLARE_META_EVENT(evChangeHour)
};
//#]
#endif // _OMINSTRUMENT

//## event evTimeTick()
class evTimeTick : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevTimeTick;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evTimeTick();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevTimeTick : virtual public AOMEvent {
    DECLARE_META_EVENT(evTimeTick)
};
//#]
#endif // _OMINSTRUMENT

//## event evChangedDispMode(DisplayMode)
class evChangedDispMode : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevChangedDispMode;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
//#[ ignore
    evChangedDispMode(int p_newMode);
//#]

    //## auto_generated
    evChangedDispMode();
    
    //## auto_generated
    evChangedDispMode(DisplayMode p_newMode);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    DisplayMode newMode;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevChangedDispMode : virtual public AOMEvent {
    DECLARE_META_EVENT(evChangedDispMode)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MicroW.h
*********************************************************************/
