/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: IEngine
//!	Generated Date	: Mon, 14, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\IEngine.h
*********************************************************************/

#ifndef IEngine_H
#define IEngine_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "MicroW.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## package MicroW

//## class IEngine
class IEngine : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedIEngine;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    IEngine(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~IEngine();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getPower() const;
    
    //## auto_generated
    void setPower(int p_power);
    
    //## auto_generated
    virtual bool startBehavior();
    
    ////    Attributes    ////

protected :

    int power;		//## attribute power
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedIEngine : virtual public AOMInstance {
    DECLARE_META(IEngine, OMAnimatedIEngine)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\IEngine.h
*********************************************************************/
