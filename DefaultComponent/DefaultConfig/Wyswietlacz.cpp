/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wyswietlacz
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Wyswietlacz.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Wyswietlacz.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define MicroW_Wyswietlacz_Wyswietlacz_SERIALIZE OM_NO_OP

#define MicroW_Wyswietlacz_clear_SERIALIZE OM_NO_OP

#define MicroW_Wyswietlacz_setDateDisp_SERIALIZE aomsmethod->addAttribute("date", x2String(date));

#define MicroW_Wyswietlacz_setNumber_SERIALIZE aomsmethod->addAttribute("num", x2String(num));

#define MicroW_Wyswietlacz_setText_SERIALIZE aomsmethod->addAttribute("text", x2String(text));

#define OMAnim_MicroW_Wyswietlacz_setDisplay_OMString_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_display)

#define OMAnim_MicroW_Wyswietlacz_setDisplay_OMString_SERIALIZE_RET_VAL

#define OMAnim_MicroW_Wyswietlacz_setDisplayMode_int_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_displayMode)

#define OMAnim_MicroW_Wyswietlacz_setDisplayMode_int_SERIALIZE_RET_VAL
//#]

//## package MicroW

//## class Wyswietlacz
Wyswietlacz::Wyswietlacz(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Wyswietlacz, Wyswietlacz(), 0, MicroW_Wyswietlacz_Wyswietlacz_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

Wyswietlacz::~Wyswietlacz() {
    NOTIFY_DESTRUCTOR(~Wyswietlacz, true);
    cleanUpRelations();
    cancelTimeouts();
}

void Wyswietlacz::clear() {
    NOTIFY_OPERATION(clear, clear(), 0, MicroW_Wyswietlacz_clear_SERIALIZE);
    //#[ operation clear()
    display = "";
    //#]
}

void Wyswietlacz::setDateDisp(int date) {
    NOTIFY_OPERATION(setDateDisp, setDateDisp(int), 1, MicroW_Wyswietlacz_setDateDisp_SERIALIZE);
    //#[ operation setDateDisp(int)
    clear();   
    char str[6]= "00:00";
    int i = 4;
    while (i >= 0)
    {          
    	if (i == 2)
    	{
    		--i;
    		continue;
    	}    
    	int c = date % 10;
    	date /= 10;
    	str[i] = '0'+ c;
    	--i;
    }
    display = str;
    //#]
}

void Wyswietlacz::setNumber(int num) {
    NOTIFY_OPERATION(setNumber, setNumber(int), 1, MicroW_Wyswietlacz_setNumber_SERIALIZE);
    //#[ operation setNumber(int)
    clear();   
    char str[6]= "00000";
    int i = 4;
    while (i >= 0)
    {              
    	int c = num % 10;
    	num /= 10;
    	str[i] = '0'+ c;
    	--i;
    }
    display = str;
    //#]
}

void Wyswietlacz::setText(const OMString& text) {
    NOTIFY_OPERATION(setText, setText(const OMString&), 1, MicroW_Wyswietlacz_setText_SERIALIZE);
    //#[ operation setText(OMString)
    display = text;
    //#]
}

int* Wyswietlacz::getDate() const {
    return date;
}

void Wyswietlacz::setDate(int* p_date) {
    date = p_date;
}

void Wyswietlacz::setDisplay(OMString p_display) {
    display = p_display;
    NOTIFY_SET_OPERATION;
}

int* Wyswietlacz::getTime() const {
    return time;
}

void Wyswietlacz::setTime(int* p_time) {
    time = p_time;
}

Sterownik* Wyswietlacz::getItsSterownik() const {
    return itsSterownik;
}

void Wyswietlacz::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Wyswietlacz::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

OMString Wyswietlacz::getDisplay() const {
    return display;
}

int Wyswietlacz::getDisplayMode() const {
    return displayMode;
}

void Wyswietlacz::setDisplayMode(int p_displayMode) {
    displayMode = p_displayMode;
    NOTIFY_SET_OPERATION;
}

void Wyswietlacz::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void Wyswietlacz::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Wyswietlacz::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Wyswietlacz::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Wyswietlacz::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Wyswietlacz::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Wyswietlacz::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Wyswietlacz::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.OFF");
        rootState_subState = OFF;
        rootState_active = OFF;
        //#[ state OFF.(Entry) 
        setDateDisp(*date);
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Wyswietlacz::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State OFF
        case OFF:
        {
            if(IS_EVENT_TYPE_OF(evChangedDispMode_MicroW_id))
                {
                    OMSETPARAMS(evChangedDispMode);
                    //## transition 1 
                    if(params->newMode ==  				STARTING)
                        {
                            NOTIFY_TRANSITION_STARTED("1");
                            NOTIFY_STATE_EXITED("ROOT.OFF");
                            //#[ transition 1 
                            
                            displayMode = params->newMode;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Starting");
                            rootState_subState = Starting;
                            rootState_active = Starting;
                            //#[ state Starting.(Entry) 
                            setText("ON");
                            //#]
                            rootState_timeout = scheduleTimeout(Sterownik::getOneSecInMs(), "ROOT.Starting");
                            NOTIFY_TRANSITION_TERMINATED("1");
                            res = eventConsumed;
                        }
                }
            else if(IS_EVENT_TYPE_OF(evDateChanged_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    NOTIFY_STATE_EXITED("ROOT.OFF");
                    NOTIFY_STATE_ENTERED("ROOT.OFF");
                    rootState_subState = OFF;
                    rootState_active = OFF;
                    //#[ state OFF.(Entry) 
                    setDateDisp(*date);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Starting
        case Starting:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("2");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Starting");
                            //#[ transition 2 
                            
                            displayMode = RUNNING;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.ON");
                            rootState_subState = ON;
                            rootState_active = ON;
                            //#[ state ON.(Entry) 
                            setNumber(*time);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State ON
        case ON:
        {
            if(IS_EVENT_TYPE_OF(evTimeTick_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.ON");
                    NOTIFY_STATE_ENTERED("ROOT.ON");
                    rootState_subState = ON;
                    rootState_active = ON;
                    //#[ state ON.(Entry) 
                    setNumber(*time);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evChangedDispMode_MicroW_id))
                {
                    OMSETPARAMS(evChangedDispMode);
                    //## transition 4 
                    if(params->newMode == 	 STOPPED)
                        {
                            NOTIFY_TRANSITION_STARTED("4");
                            NOTIFY_STATE_EXITED("ROOT.ON");
                            //#[ transition 4 
                            
                            displayMode = 
                            	params->newMode;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Stopped");
                            rootState_subState = Stopped;
                            rootState_active = Stopped;
                            //#[ state Stopped.(Entry) 
                            setText("STOP");
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("4");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 6 
                            if(params->newMode == 	 FINISHED)
                                {
                                    NOTIFY_TRANSITION_STARTED("6");
                                    NOTIFY_STATE_EXITED("ROOT.ON");
                                    //#[ transition 6 
                                    
                                    displayMode = 
                                    	params->newMode;
                                    //#]
                                    NOTIFY_STATE_ENTERED("ROOT.Done");
                                    rootState_subState = Done;
                                    rootState_active = Done;
                                    //#[ state Done.(Entry) 
                                     setText("DONE");
                                    //#]
                                    rootState_timeout = scheduleTimeout(Sterownik::getOneSecInMs()*2, "ROOT.Done");
                                    NOTIFY_TRANSITION_TERMINATED("6");
                                    res = eventConsumed;
                                }
                        }
                }
            
        }
        break;
        // State Stopped
        case Stopped:
        {
            if(IS_EVENT_TYPE_OF(evDoorClosed_MicroW_id))
                {
                    //## transition 5 
                    if((*time)>0)
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            NOTIFY_STATE_EXITED("ROOT.Stopped");
                            //#[ transition 5 
                            
                            displayMode = RUNNING;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.ON");
                            rootState_subState = ON;
                            rootState_active = ON;
                            //#[ state ON.(Entry) 
                            setNumber(*time);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 8 
                            if((*time)<=0)
                                {
                                    NOTIFY_TRANSITION_STARTED("8");
                                    NOTIFY_STATE_EXITED("ROOT.Stopped");
                                    //#[ transition 8 
                                    
                                    displayMode = OFF;
                                    //#]
                                    NOTIFY_STATE_ENTERED("ROOT.OFF");
                                    rootState_subState = OFF;
                                    rootState_active = OFF;
                                    //#[ state OFF.(Entry) 
                                    setDateDisp(*date);
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("8");
                                    res = eventConsumed;
                                }
                        }
                }
            else if(IS_EVENT_TYPE_OF(evChangedDispMode_MicroW_id))
                {
                    OMSETPARAMS(evChangedDispMode);
                    //## transition 9 
                    if(params->newMode ==OFF)
                        {
                            NOTIFY_TRANSITION_STARTED("9");
                            NOTIFY_STATE_EXITED("ROOT.Stopped");
                            //#[ transition 9 
                            
                            displayMode = 
                            	params->newMode;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.OFF");
                            rootState_subState = OFF;
                            rootState_active = OFF;
                            //#[ state OFF.(Entry) 
                            setDateDisp(*date);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("9");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Done
        case Done:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("7");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Done");
                            //#[ transition 7 
                            
                            displayMode = OFF;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.OFF");
                            rootState_subState = OFF;
                            rootState_active = OFF;
                            //#[ state OFF.(Entry) 
                            setDateDisp(*date);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("7");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedWyswietlacz::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("time", x2String(myReal->time));
    aomsAttributes->addAttribute("date", x2String(myReal->date));
    aomsAttributes->addAttribute("displayMode", x2String(myReal->displayMode));
    aomsAttributes->addAttribute("display", x2String(myReal->display));
}

void OMAnimatedWyswietlacz::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
}

void OMAnimatedWyswietlacz::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Wyswietlacz::OFF:
        {
            OFF_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::Starting:
        {
            Starting_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::ON:
        {
            ON_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::Stopped:
        {
            Stopped_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::Done:
        {
            Done_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedWyswietlacz::Stopped_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Stopped");
}

void OMAnimatedWyswietlacz::Starting_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Starting");
}

void OMAnimatedWyswietlacz::ON_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ON");
}

void OMAnimatedWyswietlacz::OFF_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.OFF");
}

void OMAnimatedWyswietlacz::Done_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Done");
}
//#]

IMPLEMENT_REACTIVE_META_P(Wyswietlacz, MicroW, MicroW, false, OMAnimatedWyswietlacz)

IMPLEMENT_META_OP(OMAnimatedWyswietlacz, MicroW_Wyswietlacz_setDisplay_OMString, "setDisplay", FALSE, "setDisplay(OMString)", 1)

IMPLEMENT_OP_CALL(MicroW_Wyswietlacz_setDisplay_OMString, Wyswietlacz, setDisplay(p_display), NO_OP())

IMPLEMENT_META_OP(OMAnimatedWyswietlacz, MicroW_Wyswietlacz_setDisplayMode_int, "setDisplayMode", FALSE, "setDisplayMode(int)", 1)

IMPLEMENT_OP_CALL(MicroW_Wyswietlacz_setDisplayMode_int, Wyswietlacz, setDisplayMode(p_displayMode), NO_OP())
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Wyswietlacz.cpp
*********************************************************************/
