/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wyswietlacz
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Wyswietlacz.h
*********************************************************************/

#ifndef Wyswietlacz_H
#define Wyswietlacz_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include "MicroW.h"
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## link itsSterownik
class Sterownik;

//#[ ignore
#define OMAnim_MicroW_Wyswietlacz_setDisplay_OMString_ARGS_DECLARATION OMString p_display;

#define OMAnim_MicroW_Wyswietlacz_setDisplayMode_int_ARGS_DECLARATION int p_displayMode;
//#]

//## package MicroW

//## class Wyswietlacz
class Wyswietlacz : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedWyswietlacz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Wyswietlacz(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Wyswietlacz();
    
    ////    Operations    ////
    
    //## operation clear()
    void clear();
    
    //## operation setDateDisp(int)
    void setDateDisp(int date);
    
    //## operation setNumber(int)
    void setNumber(int num);
    
    //## operation setText(OMString)
    void setText(const OMString& text);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int* getDate() const;
    
    //## auto_generated
    void setDate(int* p_date);
    
    //## auto_generated
    void setDisplay(OMString p_display);
    
    //## auto_generated
    int* getTime() const;
    
    //## auto_generated
    void setTime(int* p_time);
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    OMString getDisplay() const;
    
    //## auto_generated
    int getDisplayMode() const;

public :

    //## auto_generated
    void setDisplayMode(int p_displayMode);

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int* date;		//## attribute date
    
    OMString display;		//## attribute display
    
    int displayMode;		//## attribute displayMode
    
    int* time;		//## attribute time
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    ////    Framework    ////
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Stopped:
    //## statechart_method
    inline bool Stopped_IN() const;
    
    // Starting:
    //## statechart_method
    inline bool Starting_IN() const;
    
    // ON:
    //## statechart_method
    inline bool ON_IN() const;
    
    // OFF:
    //## statechart_method
    inline bool OFF_IN() const;
    
    // Done:
    //## statechart_method
    inline bool Done_IN() const;

protected :

//#[ ignore
    enum Wyswietlacz_Enum {
        OMNonState = 0,
        Stopped = 1,
        Starting = 2,
        ON = 3,
        OFF = 4,
        Done = 5
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
DECLARE_OPERATION_CLASS(MicroW_Wyswietlacz_setDisplay_OMString)

DECLARE_OPERATION_CLASS(MicroW_Wyswietlacz_setDisplayMode_int)

//#[ ignore
class OMAnimatedWyswietlacz : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Wyswietlacz, OMAnimatedWyswietlacz)
    
    DECLARE_META_OP(MicroW_Wyswietlacz_setDisplay_OMString)
    
    DECLARE_META_OP(MicroW_Wyswietlacz_setDisplayMode_int)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Stopped_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Starting_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ON_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void OFF_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Done_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Wyswietlacz::rootState_IN() const {
    return true;
}

inline bool Wyswietlacz::Stopped_IN() const {
    return rootState_subState == Stopped;
}

inline bool Wyswietlacz::Starting_IN() const {
    return rootState_subState == Starting;
}

inline bool Wyswietlacz::ON_IN() const {
    return rootState_subState == ON;
}

inline bool Wyswietlacz::OFF_IN() const {
    return rootState_subState == OFF;
}

inline bool Wyswietlacz::Done_IN() const {
    return rootState_subState == Done;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Wyswietlacz.h
*********************************************************************/
