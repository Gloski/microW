/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <aom\aom.h>
//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## classInstance itsMagnetron
#include "Magnetron.h"
//## classInstance itsRotor
#include "Rotor.h"
//## classInstance itsWyswietlacz
#include "Wyswietlacz.h"
//## auto_generated
#include "MicroW.h"
//#[ ignore
#define OMAnim_MicroW_Sterownik_setDate_int_ARGS_DECLARATION int p_date;

#define OMAnim_MicroW_Sterownik_setPowerMode_int_ARGS_DECLARATION int p_powerMode;

#define OMAnim_MicroW_Sterownik_setTime_int_ARGS_DECLARATION int p_time;
//#]

//## package MicroW

//## class Sterownik
class Sterownik : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation isProgramChanged()
    const bool isProgramChanged();
    
    //## operation ustawCzas()
    void ustawCzas();
    
    //## operation ustawProgram(int)
    void ustawProgram(int powerMode);
    
    ////    Additional operations    ////

protected :

    //## auto_generated
    int getDate() const;

public :

    //## auto_generated
    void setDate(int p_date);

protected :

    //## auto_generated
    int getPowerMode() const;

public :

    //## auto_generated
    void setPowerMode(int p_powerMode);

protected :

    //## auto_generated
    static int getPowerValues(int i1);

public :

    //## auto_generated
    void setTime(int p_time);
    
    //## auto_generated
    Magnetron* getItsMagnetron() const;
    
    //## auto_generated
    Rotor* getItsRotor() const;
    
    //## auto_generated
    Wyswietlacz* getItsWyswietlacz() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    int getTime() const;
    
    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);
    
    ////    Attributes    ////
    
    int date;		//## attribute date
    
    int powerMode;		//## attribute powerMode
    
    static int powerValues[TOTAL_N];		//## attribute powerValues
    
    int time;		//## attribute time
    
    ////    Relations and components    ////
    
    Magnetron itsMagnetron;		//## classInstance itsMagnetron
    
    Rotor itsRotor;		//## classInstance itsRotor
    
    Wyswietlacz itsWyswietlacz;		//## classInstance itsWyswietlacz
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
    //## operation Sterownik()
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## operation adjustDate()
    void adjustDate();
    
    //## operation setupWyswietlacz()
    void setupWyswietlacz();

protected :

    //## auto_generated
    static void setPowerValues(int i1, int p_powerValues);
    
    static int oneSecInMs;		//## attribute oneSecInMs

public :

    //## auto_generated
    static int getOneSecInMs();
    
    //## auto_generated
    static void setOneSecInMs(int p_oneSecInMs);
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Active:
    //## statechart_method
    inline bool Active_IN() const;
    
    //## statechart_method
    void Active_entDef();
    
    //## statechart_method
    void Active_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Active_processEvent();
    
    // state_12:
    //## statechart_method
    inline bool state_12_IN() const;
    
    //## statechart_method
    void state_12_entDef();
    
    //## statechart_method
    void state_12_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus state_12_processEvent();
    
    // Runing:
    //## statechart_method
    inline bool Runing_IN() const;
    
    // state_11:
    //## statechart_method
    inline bool state_11_IN() const;
    
    //## statechart_method
    void state_11_entDef();
    
    //## statechart_method
    void state_11_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus state_11_processEvent();
    
    // Turning_ON:
    //## statechart_method
    inline bool Turning_ON_IN() const;
    
    // Turning_OFF:
    //## statechart_method
    inline bool Turning_OFF_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Turning_OFF_handleEvent();
    
    // sendaction_14:
    //## statechart_method
    inline bool sendaction_14_IN() const;
    
    // Running:
    //## statechart_method
    inline bool Running_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Running_handleEvent();
    
    // Idle:
    //## statechart_method
    inline bool Idle_IN() const;
    
    //## statechart_method
    inline bool Idle_isCompleted();
    
    //## statechart_method
    void Idle_entDef();
    
    //## statechart_method
    void IdleEntDef();
    
    //## statechart_method
    virtual void Idle_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Idle_handleEvent();
    
    // terminationstate_8:
    //## statechart_method
    inline bool terminationstate_8_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus terminationstate_8_handleEvent();
    
    // DoorOpened:
    //## statechart_method
    inline bool DoorOpened_IN() const;

protected :

//#[ ignore
    enum Sterownik_Enum {
        OMNonState = 0,
        Active = 1,
        state_12 = 2,
        Runing = 3,
        state_11 = 4,
        Turning_ON = 5,
        Turning_OFF = 6,
        sendaction_14 = 7,
        Running = 8,
        Idle = 9,
        terminationstate_8 = 10,
        DoorOpened = 11
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int state_12_subState;
    
    int state_12_active;
    
    IOxfTimeout* state_12_timeout;
    
    int state_11_subState;
    
    int state_11_active;
    
    IOxfTimeout* state_11_timeout;
    
    int Idle_subState;
//#]
};

#ifdef _OMINSTRUMENT
DECLARE_OPERATION_CLASS(MicroW_Sterownik_setDate_int)

DECLARE_OPERATION_CLASS(MicroW_Sterownik_setPowerMode_int)

DECLARE_OPERATION_CLASS(MicroW_Sterownik_setTime_int)

//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Sterownik, OMAnimatedSterownik)
    
    DECLARE_META_OP(MicroW_Sterownik_setDate_int)
    
    DECLARE_META_OP(MicroW_Sterownik_setPowerMode_int)
    
    DECLARE_META_OP(MicroW_Sterownik_setTime_int)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Active_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void state_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Runing_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void state_11_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Turning_ON_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Turning_OFF_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_14_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Running_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Idle_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_8_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void DoorOpened_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Sterownik::rootState_IN() const {
    return true;
}

inline bool Sterownik::Active_IN() const {
    return rootState_subState == Active;
}

inline bool Sterownik::state_12_IN() const {
    return Active_IN();
}

inline bool Sterownik::Runing_IN() const {
    return state_12_subState == Runing;
}

inline bool Sterownik::state_11_IN() const {
    return Active_IN();
}

inline bool Sterownik::Turning_ON_IN() const {
    return state_11_subState == Turning_ON;
}

inline bool Sterownik::Turning_OFF_IN() const {
    return state_11_subState == Turning_OFF;
}

inline bool Sterownik::sendaction_14_IN() const {
    return state_11_subState == sendaction_14;
}

inline bool Sterownik::Running_IN() const {
    return state_11_subState == Running;
}

inline bool Sterownik::Idle_IN() const {
    return state_11_subState == Idle;
}

inline bool Sterownik::Idle_isCompleted() {
    return ( IS_IN(terminationstate_8) );
}

inline bool Sterownik::terminationstate_8_IN() const {
    return Idle_subState == terminationstate_8;
}

inline bool Sterownik::DoorOpened_IN() const {
    return state_11_subState == DoorOpened;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Sterownik.h
*********************************************************************/
