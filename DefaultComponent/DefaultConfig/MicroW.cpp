/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MicroW
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\MicroW.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "MicroW.h"
//## classInstance itsSterownik
#include "Sterownik.h"
//## auto_generated
#include "IEngine.h"
//## auto_generated
#include "Magnetron.h"
//## auto_generated
#include "Rotor.h"
//## auto_generated
#include "Wyswietlacz.h"
//#[ ignore
#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evDoorOpened_SERIALIZE OM_NO_OP

#define evDoorOpened_UNSERIALIZE OM_NO_OP

#define evDoorOpened_CONSTRUCTOR evDoorOpened()

#define evDoorClosed_SERIALIZE OM_NO_OP

#define evDoorClosed_UNSERIALIZE OM_NO_OP

#define evDoorClosed_CONSTRUCTOR evDoorClosed()

#define evDone_SERIALIZE OM_NO_OP

#define evDone_UNSERIALIZE OM_NO_OP

#define evDone_CONSTRUCTOR evDone()

#define evDateChanged_SERIALIZE OM_NO_OP

#define evDateChanged_UNSERIALIZE OM_NO_OP

#define evDateChanged_CONSTRUCTOR evDateChanged()

#define changeHour_SERIALIZE OM_NO_OP

#define changeHour_UNSERIALIZE OM_NO_OP

#define changeHour_CONSTRUCTOR changeHour()

#define evChangeHour_SERIALIZE OM_NO_OP

#define evChangeHour_UNSERIALIZE OM_NO_OP

#define evChangeHour_CONSTRUCTOR evChangeHour()

#define evTimeTick_SERIALIZE OM_NO_OP

#define evTimeTick_UNSERIALIZE OM_NO_OP

#define evTimeTick_CONSTRUCTOR evTimeTick()

#define evChangedDispMode_SERIALIZE OMADD_SER(newMode, x2String((int)myEvent->newMode))

#define evChangedDispMode_UNSERIALIZE OMADD_UNSER(int, newMode, OMDestructiveString2X)

#define evChangedDispMode_CONSTRUCTOR evChangedDispMode(newMode)
//#]

//## package MicroW


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* aomsAttributes);

static void RenameGlobalInstances();

IMPLEMENT_META_PACKAGE(MicroW, MicroW)

static void serializeGlobalVars(AOMSAttributes* aomsAttributes) {
    aomsAttributes->addAttribute("TIME_SCALE", x2String(TIME_SCALE));
}

static void RenameGlobalInstances() {
    OM_SET_INSTANCE_NAME(&itsSterownik, Sterownik, "itsSterownik", AOMNoMultiplicity);
}
#endif // _OMINSTRUMENT

//## classInstance itsSterownik
Sterownik itsSterownik;

void MicroW_initRelations() {
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    
    #ifdef _OMINSTRUMENT
    RenameGlobalInstances();
    #endif // _OMINSTRUMENT
}

bool MicroW_startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    return done;
}

//#[ ignore
MicroW_OMInitializer::MicroW_OMInitializer() {
    MicroW_initRelations();
    MicroW_startBehavior();
}

MicroW_OMInitializer::~MicroW_OMInitializer() {
}
//#]

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_MicroW_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, MicroW, MicroW, evStart())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_MicroW_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, MicroW, MicroW, evStop())

//## event evDoorOpened()
evDoorOpened::evDoorOpened() {
    NOTIFY_EVENT_CONSTRUCTOR(evDoorOpened)
    setId(evDoorOpened_MicroW_id);
}

bool evDoorOpened::isTypeOf(const short id) const {
    return (evDoorOpened_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evDoorOpened, MicroW, MicroW, evDoorOpened())

//## event evDoorClosed()
evDoorClosed::evDoorClosed() {
    NOTIFY_EVENT_CONSTRUCTOR(evDoorClosed)
    setId(evDoorClosed_MicroW_id);
}

bool evDoorClosed::isTypeOf(const short id) const {
    return (evDoorClosed_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evDoorClosed, MicroW, MicroW, evDoorClosed())

//## event evDone()
evDone::evDone() {
    NOTIFY_EVENT_CONSTRUCTOR(evDone)
    setId(evDone_MicroW_id);
}

bool evDone::isTypeOf(const short id) const {
    return (evDone_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evDone, MicroW, MicroW, evDone())

//## event evDateChanged()
evDateChanged::evDateChanged() {
    NOTIFY_EVENT_CONSTRUCTOR(evDateChanged)
    setId(evDateChanged_MicroW_id);
}

bool evDateChanged::isTypeOf(const short id) const {
    return (evDateChanged_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evDateChanged, MicroW, MicroW, evDateChanged())

//## event changeHour()
changeHour::changeHour() {
    NOTIFY_EVENT_CONSTRUCTOR(changeHour)
    setId(changeHour_MicroW_id);
}

bool changeHour::isTypeOf(const short id) const {
    return (changeHour_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(changeHour, MicroW, MicroW, changeHour())

//## event evChangeHour()
evChangeHour::evChangeHour() {
    NOTIFY_EVENT_CONSTRUCTOR(evChangeHour)
    setId(evChangeHour_MicroW_id);
}

bool evChangeHour::isTypeOf(const short id) const {
    return (evChangeHour_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evChangeHour, MicroW, MicroW, evChangeHour())

//## event evTimeTick()
evTimeTick::evTimeTick() {
    NOTIFY_EVENT_CONSTRUCTOR(evTimeTick)
    setId(evTimeTick_MicroW_id);
}

bool evTimeTick::isTypeOf(const short id) const {
    return (evTimeTick_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evTimeTick, MicroW, MicroW, evTimeTick())

//## event evChangedDispMode(DisplayMode)
//#[ ignore
evChangedDispMode::evChangedDispMode(int p_newMode) : newMode((DisplayMode)p_newMode) {
    NOTIFY_EVENT_CONSTRUCTOR(evChangedDispMode)
    setId(evChangedDispMode_MicroW_id);
}
//#]

evChangedDispMode::evChangedDispMode() {
    NOTIFY_EVENT_CONSTRUCTOR(evChangedDispMode)
    setId(evChangedDispMode_MicroW_id);
}

evChangedDispMode::evChangedDispMode(DisplayMode p_newMode) : newMode(p_newMode) {
    NOTIFY_EVENT_CONSTRUCTOR(evChangedDispMode)
    setId(evChangedDispMode_MicroW_id);
}

bool evChangedDispMode::isTypeOf(const short id) const {
    return (evChangedDispMode_MicroW_id==id);
}

IMPLEMENT_META_EVENT_P(evChangedDispMode, MicroW, MicroW, evChangedDispMode(DisplayMode))

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\MicroW.cpp
*********************************************************************/
