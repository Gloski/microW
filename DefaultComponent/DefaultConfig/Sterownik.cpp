/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 15, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Sterownik.h"
//#[ ignore
#define MicroW_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define MicroW_Sterownik_adjustDate_SERIALIZE OM_NO_OP

#define MicroW_Sterownik_isProgramChanged_SERIALIZE OM_NO_OP

#define MicroW_Sterownik_setupWyswietlacz_SERIALIZE OM_NO_OP

#define MicroW_Sterownik_ustawCzas_SERIALIZE OM_NO_OP

#define MicroW_Sterownik_ustawProgram_SERIALIZE aomsmethod->addAttribute("powerMode", x2String(powerMode));

#define OMAnim_MicroW_Sterownik_setDate_int_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_date)

#define OMAnim_MicroW_Sterownik_setDate_int_SERIALIZE_RET_VAL

#define OMAnim_MicroW_Sterownik_setPowerMode_int_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_powerMode)

#define OMAnim_MicroW_Sterownik_setPowerMode_int_SERIALIZE_RET_VAL

#define OMAnim_MicroW_Sterownik_setTime_int_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,p_time)

#define OMAnim_MicroW_Sterownik_setTime_int_SERIALIZE_RET_VAL
//#]

//## package MicroW

//## class Sterownik
int Sterownik::powerValues[TOTAL_N];

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cancelTimeouts();
}

const bool Sterownik::isProgramChanged() {
    NOTIFY_OPERATION(isProgramChanged, isProgramChanged(), 0, MicroW_Sterownik_isProgramChanged_SERIALIZE);
    //#[ operation isProgramChanged()
    if (powerValues[powerMode] != itsMagnetron.getPower())
    {
    	return true;
    }
    else
    {
    	return false;
    }
    //#]
}

void Sterownik::ustawCzas() {
    NOTIFY_OPERATION(ustawCzas, ustawCzas(), 0, MicroW_Sterownik_ustawCzas_SERIALIZE);
    //#[ operation ustawCzas()
    //#]
}

void Sterownik::ustawProgram(int powerMode) {
    NOTIFY_OPERATION(ustawProgram, ustawProgram(int), 1, MicroW_Sterownik_ustawProgram_SERIALIZE);
    //#[ operation ustawProgram(int)
    const int power = powerValues[powerMode];
    itsMagnetron.setPower(power);
    itsRotor.setPower(power);
    //#]
}

int Sterownik::getDate() const {
    return date;
}

void Sterownik::setDate(int p_date) {
    date = p_date;
    NOTIFY_SET_OPERATION;
}

int Sterownik::getPowerMode() const {
    return powerMode;
}

void Sterownik::setPowerMode(int p_powerMode) {
    powerMode = p_powerMode;
    NOTIFY_SET_OPERATION;
}

int Sterownik::getPowerValues(int i1) {
    return powerValues[i1];
}

void Sterownik::setTime(int p_time) {
    time = p_time;
    NOTIFY_SET_OPERATION;
}

Magnetron* Sterownik::getItsMagnetron() const {
    return (Magnetron*) &itsMagnetron;
}

Rotor* Sterownik::getItsRotor() const {
    return (Rotor*) &itsRotor;
}

Wyswietlacz* Sterownik::getItsWyswietlacz() const {
    return (Wyswietlacz*) &itsWyswietlacz;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsMagnetron.startBehavior();
    done &= itsRotor.startBehavior();
    done &= itsWyswietlacz.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

int Sterownik::getTime() const {
    return time;
}

void Sterownik::initRelations() {
    itsMagnetron._setItsSterownik(this);
    itsRotor._setItsSterownik(this);
    itsWyswietlacz._setItsSterownik(this);
}

void Sterownik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    state_12_subState = OMNonState;
    state_12_active = OMNonState;
    state_12_timeout = NULL;
    state_11_subState = OMNonState;
    state_11_active = OMNonState;
    state_11_timeout = NULL;
    Idle_subState = OMNonState;
}

void Sterownik::cancelTimeouts() {
    cancel(state_12_timeout);
    cancel(state_11_timeout);
}

bool Sterownik::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(state_12_timeout == arg)
        {
            state_12_timeout = NULL;
            res = true;
        }
    if(state_11_timeout == arg)
        {
            state_11_timeout = NULL;
            res = true;
        }
    return res;
}

void Sterownik::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsRotor.setActiveContext(theActiveContext, false);
        itsMagnetron.setActiveContext(theActiveContext, false);
        itsWyswietlacz.setActiveContext(theActiveContext, false);
    }
}

void Sterownik::destroy() {
    itsMagnetron.destroy();
    itsRotor.destroy();
    itsWyswietlacz.destroy();
    OMReactive::destroy();
}

Sterownik::Sterownik(IOxfActive* theActiveContext) : date(1200) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, MicroW_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsRotor.setShouldDelete(false);
        }
        {
            itsMagnetron.setShouldDelete(false);
        }
        {
            itsWyswietlacz.setShouldDelete(false);
        }
    }
    initRelations();
    initStatechart();
    //#[ operation Sterownik()
    powerValues[FREEZ]=300;
    powerValues[SLOW]=400;
    powerValues[MID]=500;
    powerValues[FAST]=600;
    powerValues[GRILL]=800;
    setupWyswietlacz();
    //#]
}

void Sterownik::adjustDate() {
    NOTIFY_OPERATION(adjustDate, adjustDate(), 0, MicroW_Sterownik_adjustDate_SERIALIZE);
    //#[ operation adjustDate()
    if (date > 2400)
    {
    	date = 0000;
    }
    else if (date % 100 > 60)
    {
    	date -= 60;
    }
    //#]
}

void Sterownik::setupWyswietlacz() {
    NOTIFY_OPERATION(setupWyswietlacz, setupWyswietlacz(), 0, MicroW_Sterownik_setupWyswietlacz_SERIALIZE);
    //#[ operation setupWyswietlacz()
    itsWyswietlacz.setDate(&date);
    itsWyswietlacz.setTime(&time);
    //#]
}

void Sterownik::setPowerValues(int i1, int p_powerValues) {
    powerValues[i1] = p_powerValues;
}

int Sterownik::oneSecInMs(TIME_SCALE * 1000);

int Sterownik::getOneSecInMs() {
    return oneSecInMs;
}

void Sterownik::setOneSecInMs(int p_oneSecInMs) {
    oneSecInMs = p_oneSecInMs;
    NOTIFY_STATIC_SET_OPERATION(Sterownik);
}

void Sterownik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        rootStateEntDef();
    }
}

void Sterownik::rootStateEntDef() {
    NOTIFY_TRANSITION_STARTED("15");
    Active_entDef();
    NOTIFY_TRANSITION_TERMINATED("15");
}

IOxfReactive::TakeEventStatus Sterownik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State Active
    if(rootState_active == Active)
        {
            res = Active_processEvent();
        }
    return res;
}

void Sterownik::Active_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active");
    rootState_subState = Active;
    rootState_active = Active;
    state_11_entDef();
    state_12_entDef();
}

void Sterownik::Active_exit() {
    state_11_exit();
    state_12_exit();
    
    NOTIFY_STATE_EXITED("ROOT.Active");
}

IOxfReactive::TakeEventStatus Sterownik::Active_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State state_11
    if(state_11_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(Active))
                {
                    return res;
                }
        }
    // State state_12
    if(state_12_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(Active))
                {
                    return res;
                }
        }
    
    return res;
}

void Sterownik::state_12_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active.state_12");
    NOTIFY_TRANSITION_STARTED("16");
    NOTIFY_STATE_ENTERED("ROOT.Active.state_12.Runing");
    state_12_subState = Runing;
    state_12_active = Runing;
    //#[ state Active.state_12.Runing.(Entry) 
    adjustDate();
    itsWyswietlacz.GEN(evDateChanged);
    //#]
    state_12_timeout = scheduleTimeout(oneSecInMs * 60, "ROOT.Active.state_12.Runing");
    NOTIFY_TRANSITION_TERMINATED("16");
}

void Sterownik::state_12_exit() {
    // State Runing
    if(state_12_subState == Runing)
        {
            cancel(state_12_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active.state_12.Runing");
        }
    state_12_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Active.state_12");
}

IOxfReactive::TakeEventStatus Sterownik::state_12_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State Runing
    if(state_12_active == Runing)
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == state_12_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("17");
                            cancel(state_12_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Active.state_12.Runing");
                            //#[ transition 17 
                            
                            ++date;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Active.state_12.Runing");
                            state_12_subState = Runing;
                            state_12_active = Runing;
                            //#[ state Active.state_12.Runing.(Entry) 
                            adjustDate();
                            itsWyswietlacz.GEN(evDateChanged);
                            //#]
                            state_12_timeout = scheduleTimeout(oneSecInMs * 60, "ROOT.Active.state_12.Runing");
                            NOTIFY_TRANSITION_TERMINATED("17");
                            res = eventConsumed;
                        }
                }
            else if(IS_EVENT_TYPE_OF(evChangeHour_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("18");
                    cancel(state_12_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active.state_12.Runing");
                    //#[ transition 18 
                    
                    ++date;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_12.Runing");
                    state_12_subState = Runing;
                    state_12_active = Runing;
                    //#[ state Active.state_12.Runing.(Entry) 
                    adjustDate();
                    itsWyswietlacz.GEN(evDateChanged);
                    //#]
                    state_12_timeout = scheduleTimeout(oneSecInMs * 60, "ROOT.Active.state_12.Runing");
                    NOTIFY_TRANSITION_TERMINATED("18");
                    res = eventConsumed;
                }
            
            
        }
    return res;
}

void Sterownik::state_11_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active.state_11");
    NOTIFY_TRANSITION_STARTED("0");
    Idle_entDef();
    NOTIFY_TRANSITION_TERMINATED("0");
}

void Sterownik::state_11_exit() {
    switch (state_11_subState) {
        // State Idle
        case Idle:
        {
            Idle_exit();
        }
        break;
        // State Running
        case Running:
        {
            popNullTransition();
            cancel(state_11_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Running");
        }
        break;
        // State Turning_ON
        case Turning_ON:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Turning_ON");
        }
        break;
        // State Turning_OFF
        case Turning_OFF:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Turning_OFF");
        }
        break;
        // State DoorOpened
        case DoorOpened:
        {
            //#[ state Active.state_11.DoorOpened.(Exit) 
            itsWyswietlacz.GEN(evDoorClosed);
            //#]
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.DoorOpened");
        }
        break;
        // State sendaction_14
        case sendaction_14:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.sendaction_14");
        }
        break;
        default:
            break;
    }
    state_11_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Active.state_11");
}

IOxfReactive::TakeEventStatus Sterownik::state_11_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (state_11_active) {
        // State terminationstate_8
        case terminationstate_8:
        {
            res = terminationstate_8_handleEvent();
        }
        break;
        // State Running
        case Running:
        {
            res = Running_handleEvent();
        }
        break;
        // State Turning_ON
        case Turning_ON:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.Turning_ON");
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Running");
                    pushNullTransition();
                    state_11_subState = Running;
                    state_11_active = Running;
                    state_11_timeout = scheduleTimeout(oneSecInMs, "ROOT.Active.state_11.Running");
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Turning_OFF
        case Turning_OFF:
        {
            res = Turning_OFF_handleEvent();
        }
        break;
        // State DoorOpened
        case DoorOpened:
        {
            if(IS_EVENT_TYPE_OF(evDoorClosed_MicroW_id))
                {
                    //## transition 3 
                    if(time > 0)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            //#[ state Active.state_11.DoorOpened.(Exit) 
                            itsWyswietlacz.GEN(evDoorClosed);
                            //#]
                            NOTIFY_STATE_EXITED("ROOT.Active.state_11.DoorOpened");
                            NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Turning_ON");
                            pushNullTransition();
                            state_11_subState = Turning_ON;
                            state_11_active = Turning_ON;
                            //#[ state Active.state_11.Turning_ON.(Entry) 
                            itsMagnetron.GEN(evStart);
                            itsRotor.GEN(evStart);
                            itsWyswietlacz.GEN(evChangedDispMode(STARTING));
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 7 
                            if(time <= 0)
                                {
                                    NOTIFY_TRANSITION_STARTED("7");
                                    //#[ state Active.state_11.DoorOpened.(Exit) 
                                    itsWyswietlacz.GEN(evDoorClosed);
                                    //#]
                                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.DoorOpened");
                                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Turning_OFF");
                                    pushNullTransition();
                                    state_11_subState = Turning_OFF;
                                    state_11_active = Turning_OFF;
                                    //#[ state Active.state_11.Turning_OFF.(Entry) 
                                    itsMagnetron.GEN(evStop);
                                    itsRotor.GEN(evStop);
                                    itsWyswietlacz.GEN(evChangedDispMode(STOPPED));
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("7");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        // State sendaction_14
        case sendaction_14:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.sendaction_14");
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Running");
                    pushNullTransition();
                    state_11_subState = Running;
                    state_11_active = Running;
                    state_11_timeout = scheduleTimeout(oneSecInMs, "ROOT.Active.state_11.Running");
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Turning_OFF_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 6 
            if(time > 0)
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.Turning_OFF");
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.DoorOpened");
                    state_11_subState = DoorOpened;
                    state_11_active = DoorOpened;
                    //#[ state Active.state_11.DoorOpened.(Entry) 
                    itsWyswietlacz.GEN(evDoorOpened);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            else
                {
                    //## transition 8 
                    if(time <= 0)
                        {
                            NOTIFY_TRANSITION_STARTED("8");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Turning_OFF");
                            //#[ transition 8 
                            
                            itsWyswietlacz.
                            	GEN(evChangedDispMode(OFF));
                            //#]
                            Idle_entDef();
                            NOTIFY_TRANSITION_TERMINATED("8");
                            res = eventConsumed;
                        }
                }
        }
    
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::Running_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == state_11_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    cancel(state_11_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.Running");
                    //#[ transition 4 
                    
                    --time;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.sendaction_14");
                    pushNullTransition();
                    state_11_subState = sendaction_14;
                    state_11_active = sendaction_14;
                    //#[ state Active.state_11.sendaction_14.(Entry) 
                    itsWyswietlacz.GEN(evTimeTick);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 9 
            if(time <= 0)
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    cancel(state_11_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Active.state_11.Running");
                    //#[ transition 9 
                    
                    itsWyswietlacz.GEN(evChangedDispMode(FINISHED));
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Turning_OFF");
                    pushNullTransition();
                    state_11_subState = Turning_OFF;
                    state_11_active = Turning_OFF;
                    //#[ state Active.state_11.Turning_OFF.(Entry) 
                    itsMagnetron.GEN(evStop);
                    itsRotor.GEN(evStop);
                    itsWyswietlacz.GEN(evChangedDispMode(STOPPED));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(evDoorOpened_MicroW_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            popNullTransition();
            cancel(state_11_timeout);
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Running");
            NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Turning_OFF");
            pushNullTransition();
            state_11_subState = Turning_OFF;
            state_11_active = Turning_OFF;
            //#[ state Active.state_11.Turning_OFF.(Entry) 
            itsMagnetron.GEN(evStop);
            itsRotor.GEN(evStop);
            itsWyswietlacz.GEN(evChangedDispMode(STOPPED));
            //#]
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    
    
    return res;
}

void Sterownik::Idle_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Idle");
    pushNullTransition();
    state_11_subState = Idle;
    state_11_timeout = scheduleTimeout(100, "ROOT.Active.state_11.Idle");
    IdleEntDef();
}

void Sterownik::IdleEntDef() {
    //## transition 11 
    if(isProgramChanged() == true)
        {
            NOTIFY_TRANSITION_STARTED("12");
            NOTIFY_TRANSITION_STARTED("11");
            //#[ transition 11 
            
            ustawProgram(powerMode);
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Idle.terminationstate_8");
            Idle_subState = terminationstate_8;
            state_11_active = terminationstate_8;
            NOTIFY_TRANSITION_TERMINATED("11");
            NOTIFY_TRANSITION_TERMINATED("12");
        }
    else
        {
            NOTIFY_TRANSITION_STARTED("12");
            NOTIFY_TRANSITION_STARTED("13");
            NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Idle.terminationstate_8");
            Idle_subState = terminationstate_8;
            state_11_active = terminationstate_8;
            NOTIFY_TRANSITION_TERMINATED("13");
            NOTIFY_TRANSITION_TERMINATED("12");
        }
}

void Sterownik::Idle_exit() {
    popNullTransition();
    // State terminationstate_8
    if(Idle_subState == terminationstate_8)
        {
            NOTIFY_STATE_EXITED("ROOT.Active.state_11.Idle.terminationstate_8");
        }
    Idle_subState = OMNonState;
    cancel(state_11_timeout);
    NOTIFY_STATE_EXITED("ROOT.Active.state_11.Idle");
}

IOxfReactive::TakeEventStatus Sterownik::Idle_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == state_11_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("10");
                    Idle_exit();
                    Idle_entDef();
                    NOTIFY_TRANSITION_TERMINATED("10");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 1 
            if(time > 0 && (IS_COMPLETED(Idle)==true))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    Idle_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Active.state_11.Turning_ON");
                    pushNullTransition();
                    state_11_subState = Turning_ON;
                    state_11_active = Turning_ON;
                    //#[ state Active.state_11.Turning_ON.(Entry) 
                    itsMagnetron.GEN(evStart);
                    itsRotor.GEN(evStart);
                    itsWyswietlacz.GEN(evChangedDispMode(STARTING));
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(evDoorOpened_MicroW_id))
        {
            NOTIFY_TRANSITION_STARTED("14");
            Idle_exit();
            NOTIFY_STATE_ENTERED("ROOT.Active.state_11.DoorOpened");
            state_11_subState = DoorOpened;
            state_11_active = DoorOpened;
            //#[ state Active.state_11.DoorOpened.(Entry) 
            itsWyswietlacz.GEN(evDoorOpened);
            //#]
            NOTIFY_TRANSITION_TERMINATED("14");
            res = eventConsumed;
        }
    
    
    return res;
}

IOxfReactive::TakeEventStatus Sterownik::terminationstate_8_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    res = Idle_handleEvent();
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("time", x2String(myReal->time));
    aomsAttributes->addAttribute("powerMode", x2String(myReal->powerMode));
    aomsAttributes->addAttribute("date", x2String(myReal->date));
    aomsAttributes->addAttribute("powerValues", array2String(TOTAL_N, myReal->powerValues, sizeof(int), &x2String));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsRotor", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsRotor);
    aomsRelations->addRelation("itsMagnetron", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsMagnetron);
    aomsRelations->addRelation("itsWyswietlacz", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsWyswietlacz);
}

void OMAnimatedSterownik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    if(myReal->rootState_subState == Sterownik::Active)
        {
            Active_serializeStates(aomsState);
        }
}

void OMAnimatedSterownik::Active_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active");
    state_11_serializeStates(aomsState);
    state_12_serializeStates(aomsState);
}

void OMAnimatedSterownik::state_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_12");
    if(myReal->state_12_subState == Sterownik::Runing)
        {
            Runing_serializeStates(aomsState);
        }
}

void OMAnimatedSterownik::Runing_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_12.Runing");
}

void OMAnimatedSterownik::state_11_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11");
    switch (myReal->state_11_subState) {
        case Sterownik::Idle:
        {
            Idle_serializeStates(aomsState);
        }
        break;
        case Sterownik::Running:
        {
            Running_serializeStates(aomsState);
        }
        break;
        case Sterownik::Turning_ON:
        {
            Turning_ON_serializeStates(aomsState);
        }
        break;
        case Sterownik::Turning_OFF:
        {
            Turning_OFF_serializeStates(aomsState);
        }
        break;
        case Sterownik::DoorOpened:
        {
            DoorOpened_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_14:
        {
            sendaction_14_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::Turning_ON_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.Turning_ON");
}

void OMAnimatedSterownik::Turning_OFF_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.Turning_OFF");
}

void OMAnimatedSterownik::sendaction_14_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.sendaction_14");
}

void OMAnimatedSterownik::Running_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.Running");
}

void OMAnimatedSterownik::Idle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.Idle");
    if(myReal->Idle_subState == Sterownik::terminationstate_8)
        {
            terminationstate_8_serializeStates(aomsState);
        }
}

void OMAnimatedSterownik::terminationstate_8_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.Idle.terminationstate_8");
}

void OMAnimatedSterownik::DoorOpened_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Active.state_11.DoorOpened");
}
//#]

IMPLEMENT_REACTIVE_META_P(Sterownik, MicroW, MicroW, false, OMAnimatedSterownik)

IMPLEMENT_META_OP(OMAnimatedSterownik, MicroW_Sterownik_setDate_int, "setDate", FALSE, "setDate(int)", 1)

IMPLEMENT_OP_CALL(MicroW_Sterownik_setDate_int, Sterownik, setDate(p_date), NO_OP())

IMPLEMENT_META_OP(OMAnimatedSterownik, MicroW_Sterownik_setPowerMode_int, "setPowerMode", FALSE, "setPowerMode(int)", 1)

IMPLEMENT_OP_CALL(MicroW_Sterownik_setPowerMode_int, Sterownik, setPowerMode(p_powerMode), NO_OP())

IMPLEMENT_META_OP(OMAnimatedSterownik, MicroW_Sterownik_setTime_int, "setTime", FALSE, "setTime(int)", 1)

IMPLEMENT_OP_CALL(MicroW_Sterownik_setTime_int, Sterownik, setTime(p_time), NO_OP())
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Sterownik.cpp
*********************************************************************/
