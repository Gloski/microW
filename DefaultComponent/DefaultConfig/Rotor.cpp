/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Rotor
//!	Generated Date	: Mon, 14, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Rotor.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Rotor.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define MicroW_Rotor_Rotor_SERIALIZE OM_NO_OP
//#]

//## package MicroW

//## class Rotor
Rotor::Rotor(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Rotor, Rotor(), 0, MicroW_Rotor_Rotor_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

Rotor::~Rotor() {
    NOTIFY_DESTRUCTOR(~Rotor, false);
    cleanUpRelations();
    cancelTimeouts();
}

void Rotor::setAngle(int p_angle) {
    angle = p_angle;
    NOTIFY_SET_OPERATION;
}

Sterownik* Rotor::getItsSterownik() const {
    return itsSterownik;
}

void Rotor::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Rotor::startBehavior() {
    bool done = false;
    done = IEngine::startBehavior();
    return done;
}

int Rotor::getAngle() const {
    return angle;
}

void Rotor::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void Rotor::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Rotor::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Rotor::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void Rotor::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Rotor::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Rotor::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Rotor::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Idle");
        rootState_subState = Idle;
        rootState_active = Idle;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Rotor::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Idle
        case Idle:
        {
            if(IS_EVENT_TYPE_OF(evStart_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Idle");
                    NOTIFY_STATE_ENTERED("ROOT.Rotate");
                    rootState_subState = Rotate;
                    rootState_active = Rotate;
                    rootState_timeout = scheduleTimeout(200, "ROOT.Rotate");
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Rotate
        case Rotate:
        {
            if(IS_EVENT_TYPE_OF(evStop_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    cancel(rootState_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Rotate");
                    NOTIFY_STATE_ENTERED("ROOT.Idle");
                    rootState_subState = Idle;
                    rootState_active = Idle;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Rotate");
                            //#[ transition 3 
                            
                            angle >= 360?
                            	angle = 2 * TIME_SCALE:
                            	angle += 2 * TIME_SCALE;
                            //#]
                            NOTIFY_STATE_ENTERED("ROOT.Rotate");
                            rootState_subState = Rotate;
                            rootState_active = Rotate;
                            rootState_timeout = scheduleTimeout(200, "ROOT.Rotate");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedRotor::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("angle", x2String(myReal->angle));
    OMAnimatedIEngine::serializeAttributes(aomsAttributes);
}

void OMAnimatedRotor::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedIEngine::serializeRelations(aomsRelations);
}

void OMAnimatedRotor::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Rotor::Idle:
        {
            Idle_serializeStates(aomsState);
        }
        break;
        case Rotor::Rotate:
        {
            Rotate_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedRotor::Rotate_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Rotate");
}

void OMAnimatedRotor::Idle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Idle");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Rotor, MicroW, false, IEngine, OMAnimatedIEngine, OMAnimatedRotor)

OMINIT_SUPERCLASS(IEngine, OMAnimatedIEngine)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Rotor.cpp
*********************************************************************/
