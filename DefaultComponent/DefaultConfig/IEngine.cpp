/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: IEngine
//!	Generated Date	: Mon, 14, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\IEngine.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "IEngine.h"
//#[ ignore
#define MicroW_IEngine_IEngine_SERIALIZE OM_NO_OP
//#]

//## package MicroW

//## class IEngine
IEngine::IEngine(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(IEngine, IEngine(), 0, MicroW_IEngine_IEngine_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

IEngine::~IEngine() {
    NOTIFY_DESTRUCTOR(~IEngine, true);
}

int IEngine::getPower() const {
    return power;
}

void IEngine::setPower(int p_power) {
    power = p_power;
    NOTIFY_SET_OPERATION;
}

bool IEngine::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedIEngine::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("power", x2String(myReal->power));
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(IEngine, MicroW, MicroW, false, OMAnimatedIEngine)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\IEngine.cpp
*********************************************************************/
