/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Mon, 7, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## classInstance itsSterownik
#include "Sterownik.h"
//## auto_generated
#include "IEngine.h"
//## auto_generated
#include "Magnetron.h"
//## auto_generated
#include "Rotor.h"
//## auto_generated
#include "Wyswietlacz.h"
//#[ ignore
#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evDoorOpened_SERIALIZE OM_NO_OP

#define evDoorOpened_UNSERIALIZE OM_NO_OP

#define evDoorOpened_CONSTRUCTOR evDoorOpened()

#define evDoorClosed_SERIALIZE OM_NO_OP

#define evDoorClosed_UNSERIALIZE OM_NO_OP

#define evDoorClosed_CONSTRUCTOR evDoorClosed()

#define evDone_SERIALIZE OM_NO_OP

#define evDone_UNSERIALIZE OM_NO_OP

#define evDone_CONSTRUCTOR evDone()

#define evDateChanged_SERIALIZE OM_NO_OP

#define evDateChanged_UNSERIALIZE OM_NO_OP

#define evDateChanged_CONSTRUCTOR evDateChanged()

#define evDataChanged_SERIALIZE OM_NO_OP

#define evDataChanged_UNSERIALIZE OM_NO_OP

#define evDataChanged_CONSTRUCTOR evDataChanged()

#define changeHour_SERIALIZE OM_NO_OP

#define changeHour_UNSERIALIZE OM_NO_OP

#define changeHour_CONSTRUCTOR changeHour()

#define evChangeHour_SERIALIZE OM_NO_OP

#define evChangeHour_UNSERIALIZE OM_NO_OP

#define evChangeHour_CONSTRUCTOR evChangeHour()
//#]

//## package Default


//## classInstance itsSterownik
Sterownik itsSterownik;

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* aomsAttributes);

static void RenameGlobalInstances();

IMPLEMENT_META_PACKAGE(Default, Default)
#endif // _OMINSTRUMENT

void Default_initRelations() {
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    
    #ifdef _OMINSTRUMENT
    RenameGlobalInstances();
    #endif // _OMINSTRUMENT
}

bool Default_startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* aomsAttributes) {
    aomsAttributes->addAttribute("TIME_SCALE", x2String(TIME_SCALE));
}

static void RenameGlobalInstances() {
    OM_SET_INSTANCE_NAME(&itsSterownik, Sterownik, "itsSterownik", AOMNoMultiplicity);
}
#endif // _OMINSTRUMENT

//#[ ignore
Default_OMInitializer::Default_OMInitializer() {
    Default_initRelations();
    Default_startBehavior();
}

Default_OMInitializer::~Default_OMInitializer() {
}
//#]

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evDoorOpened()
evDoorOpened::evDoorOpened() {
    NOTIFY_EVENT_CONSTRUCTOR(evDoorOpened)
    setId(evDoorOpened_Default_id);
}

bool evDoorOpened::isTypeOf(const short id) const {
    return (evDoorOpened_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDoorOpened, Default, Default, evDoorOpened())

//## event evDoorClosed()
evDoorClosed::evDoorClosed() {
    NOTIFY_EVENT_CONSTRUCTOR(evDoorClosed)
    setId(evDoorClosed_Default_id);
}

bool evDoorClosed::isTypeOf(const short id) const {
    return (evDoorClosed_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDoorClosed, Default, Default, evDoorClosed())

//## event evDone()
evDone::evDone() {
    NOTIFY_EVENT_CONSTRUCTOR(evDone)
    setId(evDone_Default_id);
}

bool evDone::isTypeOf(const short id) const {
    return (evDone_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDone, Default, Default, evDone())

//## event evDateChanged()
evDateChanged::evDateChanged() {
    NOTIFY_EVENT_CONSTRUCTOR(evDateChanged)
    setId(evDateChanged_Default_id);
}

bool evDateChanged::isTypeOf(const short id) const {
    return (evDateChanged_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDateChanged, Default, Default, evDateChanged())

//## event evDataChanged()
evDataChanged::evDataChanged() {
    NOTIFY_EVENT_CONSTRUCTOR(evDataChanged)
    setId(evDataChanged_Default_id);
}

bool evDataChanged::isTypeOf(const short id) const {
    return (evDataChanged_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDataChanged, Default, Default, evDataChanged())

//## event changeHour()
changeHour::changeHour() {
    NOTIFY_EVENT_CONSTRUCTOR(changeHour)
    setId(changeHour_Default_id);
}

bool changeHour::isTypeOf(const short id) const {
    return (changeHour_Default_id==id);
}

IMPLEMENT_META_EVENT_P(changeHour, Default, Default, changeHour())

//## event evChangeHour()
evChangeHour::evChangeHour() {
    NOTIFY_EVENT_CONSTRUCTOR(evChangeHour)
    setId(evChangeHour_Default_id);
}

bool evChangeHour::isTypeOf(const short id) const {
    return (evChangeHour_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evChangeHour, Default, Default, evChangeHour())

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Default.cpp
*********************************************************************/
