/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: User
//!	Generated Date	: Mon, 14, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\User.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "User.h"
//#[ ignore
#define MicroW_User_User_SERIALIZE OM_NO_OP
//#]

//## package MicroW

//## actor User
User::User() {
    NOTIFY_CONSTRUCTOR(User, User(), 0, MicroW_User_User_SERIALIZE);
}

User::~User() {
    NOTIFY_DESTRUCTOR(~User, true);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUser::serializeRelations(AOMSRelations* aomsRelations) const {
}
//#]

IMPLEMENT_META_P(User, MicroW, MicroW, false, OMAnimatedUser)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\User.cpp
*********************************************************************/
