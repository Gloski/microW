/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Magnetron
//!	Generated Date	: Mon, 14, Jun 2021  
	File Path	: DefaultComponent\DefaultConfig\Magnetron.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Magnetron.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define MicroW_Magnetron_Magnetron_SERIALIZE OM_NO_OP

#define MicroW_Magnetron_ustawMoc_SERIALIZE OM_NO_OP
//#]

//## package MicroW

//## class Magnetron
Magnetron::Magnetron(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Magnetron, Magnetron(), 0, MicroW_Magnetron_Magnetron_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

Magnetron::~Magnetron() {
    NOTIFY_DESTRUCTOR(~Magnetron, false);
    cleanUpRelations();
}

void Magnetron::ustawMoc() {
    NOTIFY_OPERATION(ustawMoc, ustawMoc(), 0, MicroW_Magnetron_ustawMoc_SERIALIZE);
    //#[ operation ustawMoc()
    //#]
}

Sterownik* Magnetron::getItsSterownik() const {
    return itsSterownik;
}

void Magnetron::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Magnetron::startBehavior() {
    bool done = false;
    done = IEngine::startBehavior();
    return done;
}

void Magnetron::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Magnetron::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Magnetron::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Magnetron::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Magnetron::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void Magnetron::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Idle");
        rootState_subState = Idle;
        rootState_active = Idle;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Magnetron::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Idle
        case Idle:
        {
            if(IS_EVENT_TYPE_OF(evStart_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Idle");
                    NOTIFY_STATE_ENTERED("ROOT.Running");
                    rootState_subState = Running;
                    rootState_active = Running;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Running
        case Running:
        {
            if(IS_EVENT_TYPE_OF(evStop_MicroW_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Running");
                    NOTIFY_STATE_ENTERED("ROOT.Idle");
                    rootState_subState = Idle;
                    rootState_active = Idle;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedMagnetron::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedIEngine::serializeAttributes(aomsAttributes);
}

void OMAnimatedMagnetron::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedIEngine::serializeRelations(aomsRelations);
}

void OMAnimatedMagnetron::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Magnetron::Idle:
        {
            Idle_serializeStates(aomsState);
        }
        break;
        case Magnetron::Running:
        {
            Running_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedMagnetron::Running_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Running");
}

void OMAnimatedMagnetron::Idle_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Idle");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Magnetron, MicroW, false, IEngine, OMAnimatedIEngine, OMAnimatedMagnetron)

OMINIT_SUPERCLASS(IEngine, OMAnimatedIEngine)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Magnetron.cpp
*********************************************************************/
